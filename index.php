<!DOCTYPE HTML>
  <html lang="pl">
 	 <head>
		<meta charset="utf-8">
		<title>Rejestracja</title>
		
		<meta name="description" content="description"/>
		<meta name="keywors" content="programming" />
		<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=edge,chrome=1"/>

		<link rel="stylesheet" href="style.css" type="text/css" />
		

 	</head>
  
	<body>
		<h1>Rejestracja na Warsztaty Giełdy Pasji i Umiejętności!</h1><br>
		
		<form id='form'>
			Numer legitymacji szkolnej<input type='text' id ='nr'><br>
			<br><button id='zaloguj'>Zaloguj!</button>
		</form>
		<b><div id = 'odp'></div></b><br>
			<b>Lista Warsztatów: </b>
			<?php
				require_once("connect.php");
				$conn = new mysqli($servername, $username, $password, $db_name);

				$conn->query('SET NAMES utf8');
				$conn->query("SET CHARACTER_SET utf8_general_ci");
				$sqlquery = "SELECT * FROM warsztaty";
				$result = $conn->query($sqlquery);
		
				
				echo "<br>";
				echo "<table id = 'tabela' >";
				echo "<tr><td>Numer</td><td>Prowadzący</td><td>Nazwa</td><td>Gabinet</td><td>Tura I</td><td>Miejsca wolne na turę I</td><td>Tura II</td><td>Miejsca wolne na turę II</td><td>Numer</td></tr>";
				while($row = $result->fetch_array()) {
					echo "<tr>";
						$wolne1 = $row[6]- $conn->query("SELECT id FROM uczniowie WHERE tura1=".$row[0])->num_rows;
						$wolne2 = $row[6]- $conn->query("SELECT id FROM uczniowie WHERE tura2=".$row[0])->num_rows;

						echo "<td>".$row[0]."</td>";
						echo "<td>".$row[1]."</td>";
						echo "<td>".$row[2]."</td>";
						echo "<td>".$row[3]."</td>";
						echo "<td>";
						if($row[4] == 1) echo "<button id = '1-".$row[0]."' onClick = 'register(this.id)'>Zarejestruj</button>";
						echo "</td>";
						echo "<td>".($wolne1)."</td>";
						echo "<td>";
						if($row[5] == 1) echo "<button id = '2-".$row[0]."' onClick = 'register(this.id)'>Zarejestruj</button>";
						echo "</td>";
						echo "<td>".($wolne2)."</td>";
						echo "<td>".$row[0]."</td>";
					echo "</tr>";
				}
				echo "</table>";
			?>
			
		<script src="main.js"></script>
	</body>
  
</html>
