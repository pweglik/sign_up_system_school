<?php
	session_start();

	require_once("connect.php");
	$conn = new mysqli($servername, $username, $password, $db_name);

	header("Content-Type: application/json; charset=UTF-8");

	connect_db($conn);
	fetchData($conn);

	
	function connect_db($conn)
	{
		if($conn -> connect_error)
		{
			die($conn->errno." ".$conn->error);
		}
	}

	

	function fetchData($conn)
	{
		$sqlquery = "SELECT con, beg, ending FROM starlines";

		$result = $conn->query($sqlquery);

		if($result->num_rows>0)
		{	
        	while($row = $result->fetch_array()) {
   				 $data[] = $row; 
			}

			echo json_encode($data);	
		}
		
		//$result ->free_result(); 
	}

	
	$conn->close();
?>